//activity file

const http = require('http');

const port = 3000;

const server = http.createServer((reQ, reS) => 
{
  if (reQ.url === '/login') 
  {
    reS.writeHead(200, {'Content-Type': 'text/plain'});
    reS.end('You are in the login page');
  } 
    else  {
        reS.writeHead(200, {'Content-Type': 'text/plain'});
        reS.end('Im sorry the page you are looking for cannot be found'); 
          }
});

server.listen(port, () => {
  console.log(`Server is Successfully Running ${port}`);
});
