// 1. What directive is used by Node.js in loading the modules it needs?

    -  require(); directive is used to load module


// 2. What Node.js module contains a method for server creation?

    -  http module contains "function" example http.createServer((request, response)

// 3. What is the method of the http object responsible for creating a server using Node.js?  

    -  createServer() is the method use for creating serves using Node.js


// 4. What method of the response object allows us to set status codes and content types?
	
    - writeHead() is a method used to set a status code and content

// 5. Where will console.log() output its contents when run in Node.js?

   -  The output console.log() will be seen in git bash when using gitbash

// 6. What property of the request object contains the address's endpoint?

   -  URL  is the property of the request object that contains the address endpoint..
